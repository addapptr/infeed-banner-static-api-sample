//
//  ViewController.swift
//  StaticInfeedBanner
//
//  Created by Mohamed Matloub on 4/29/21.
//

import UIKit
import AATKit

enum InfeedType {
	case author(author: String)
	case ad
}

class ViewController: UIViewController {
	@IBOutlet weak private var tableView: UITableView!
	// MARK: [ADS] - Ads infeed configuration
    var stickyBannerPlacement: AATStickyBannerPlacement?


	// You might consider adjusting these parameters depending on the device type (screen height)
	fileprivate var adsFirstIndex = 2
	fileprivate var adsRecursivity = 15

	private var data: [InfeedType] = []

	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.delegate = self
		tableView.dataSource = self
		tableView.rowHeight = UITableView.automaticDimension

        stickyBannerPlacement = AATSDK.createStickyBannerPlacement(name: "StickyBannerPlacement", size: .banner320x53)
        stickyBannerPlacement?.delegate = self

		prepareData()
	}

	func prepareData() {
		let arr = authors.compactMap({ InfeedType.author(author: $0) })
		var infeeedArr = arr
		for index in stride(from: self.adsFirstIndex, to: arr.count, by: self.adsRecursivity) {
			infeeedArr.insert(.ad, at: index)
		}
		self.data = infeeedArr
		self.tableView.reloadData()
	}
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // [ADS] - Refresh Ads for placement
        // [ADS] you may set keyword targeting here for placement
        AATSDK.controllerViewDidAppear(controller: self)
        stickyBannerPlacement?.startAutoReload()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // [ADS] - stop auto reloading for placement "ProgramPage"
        stickyBannerPlacement?.stopAutoReload()
        AATSDK.controllerViewWillDisappear()
    }
}

extension ViewController: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let current = data[indexPath.item]
		switch current {
		case .author(let author):
			let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? InfeedTableViewCell
			cell?.titleLabel.text = author
			return cell ?? UITableViewCell()

		case .ad:
			// [ADS]  manage presentation of "InFeed" ad view
			let cell = tableView.dequeueReusableCell(withIdentifier: "adCell", for: indexPath) as? AdTableViewCell
			return cell ?? UITableViewCell()
		}
	}

	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		if let adCell = cell as? AdTableViewCell,
		   tableView.indexPathsForVisibleRows?.contains(indexPath) == true, // prevent the ad view from being positioned incorrectly when the view controller is rendered for a first time
		   let placement = self.stickyBannerPlacement,
           let adView = placement.getPlacementView() {
			//Adding adView to cell
			adCell.addAdView(adView)
		}
	}

	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		let current = data[indexPath.item]
		switch current {
		case .author:
			return UITableView.automaticDimension
		case .ad:
			return 250
		}
	}
}

extension ViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

	}
}

extension ViewController: AATStatisticsDelegate {
    func AATKitCountedNetworkImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }

    func AATKitCountedMediationCycle(placement: AATPlacement?) {
        print(#function)
    }
    
    func AATKitCountedAdSpace(placement: AATPlacement?) {
        print(#function)
    }
    
    func AATKitCountedRequest(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedResponse(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedVImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedClick(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
    
    func AATKitCountedDirectDealImpression(placement: AATPlacement?, for network: AATAdNetwork) {
        print(#function)
    }
}

extension ViewController: AATStickyBannerPlacementDelegate {
    func aatPauseForAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatResumeAfterAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatHaveAd(placement: AATPlacement) {
        print(#function)
    }
    
    func aatNoAd(placement: AATPlacement) {
        print(#function)
    }
}
