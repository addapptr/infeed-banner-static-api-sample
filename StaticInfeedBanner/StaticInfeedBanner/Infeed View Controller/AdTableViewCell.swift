//
//  AdTableViewCell.swift
//  StaticInfeedBanner
//
//  Created by Mohamed Matloub on 5/4/21.
//

import UIKit

class AdTableViewCell: UITableViewCell {
	@IBOutlet weak var adContainerView: UIView!

	override func prepareForReuse() {
		super.prepareForReuse()
		adContainerView.subviews.forEach({ $0.removeFromSuperview() })
	}
	
	func addAdView(_ adView: UIView) {
		//Adding adView to cell
		adView.removeFromSuperview()
		adContainerView.addSubview(adView)

		// adding constraints to adView
		adView.translatesAutoresizingMaskIntoConstraints = false
		adContainerView.centerXAnchor.constraint(equalTo: adView.centerXAnchor).isActive = true
		adContainerView.centerYAnchor.constraint(equalTo: adView.centerYAnchor).isActive = true

		adView.heightAnchor.constraint(equalToConstant: adView.frame.height).isActive = true
		adView.widthAnchor.constraint(equalToConstant: adView.frame.width).isActive = true
	}
}
