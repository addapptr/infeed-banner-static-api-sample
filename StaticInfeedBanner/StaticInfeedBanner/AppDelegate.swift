//
//  AppDelegate.swift
//  StaticInfeedBanner
//
//  Created by Mohamed Matloub on 6/4/21.
//

import UIKit
import AATKit
import AppTrackingTransparency

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.

		// !IMPORTANT! Don't forget to set your own Google AppId in info.plist
		let configuration = AATConfiguration()

		// !IMPORTANT! this line for this demo purpose only and shouldn't be used in live apps
		configuration.testModeAccountId = 1995

        AATSDK.initAATKit(with: configuration)

        AATSDK.setLogLevel(logLevel: .debug)
        
        // if you want to be notified by statistics reports request parameters,
        // you need to conform to AATReportsDelegate,
        AATSDK.setReportsDelegate(self)
        
        // Start from iOS 14.5, you need to request Tracking Autherization in order to recieve personalized Ads
        ATTrackingManager.requestTrackingAuthorization { status in
            // Tracking authorization completed. Start loading ads here
        }
        
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

extension AppDelegate: AATDelegate {
	func aatKitObtainedAdRules(_ fromTheServer: Bool) {
		print("Got rules")
		NotificationCenter.default.post(name: NSNotification.Name("aatKitObtainedAdRules"), object: nil)
	}
}

extension AppDelegate: AATReportsDelegate {
    func onReportSent(_ report: String) {
        print("\(#function) -> \(report)")
    }
}
